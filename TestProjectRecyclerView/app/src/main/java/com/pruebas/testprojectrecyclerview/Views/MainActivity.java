package com.pruebas.testprojectrecyclerview.Views;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.pruebas.testprojectrecyclerview.Controllers.GalleryAdapter;
import com.pruebas.testprojectrecyclerview.Models.Galeria;
import com.pruebas.testprojectrecyclerview.R;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private RecyclerView rcGalleries;
    private GalleryAdapter galleryAdapter;
    private LinearLayoutManager lnLyManager;
    private List<Galeria> LsGalerries;

    private Button btnNewGallery, btnAddGallery;
    private LinearLayout lyNewGallery;

    private EditText txtGalleryName, txtGalleryDescription, txtGalleryLatitude, txtGalleryLongitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.rcGalleries = (RecyclerView) findViewById(R.id.rcGalleries);
        this.rcGalleries.setHasFixedSize(true);

        lnLyManager = new LinearLayoutManager(this);
        rcGalleries.setLayoutManager(lnLyManager);

        LsGalerries = new ArrayList<>();

        galleryAdapter = new GalleryAdapter(this, LsGalerries);
        rcGalleries.setAdapter(galleryAdapter);

        btnNewGallery = (Button) findViewById(R.id.btnNewGallery);
        btnAddGallery = (Button) findViewById(R.id.btnAddGallery);

        lyNewGallery = (LinearLayout) findViewById(R.id.lyNewGallery);

        txtGalleryName = (EditText) findViewById(R.id.txtGalleryName);
        txtGalleryDescription = (EditText) findViewById(R.id.txtGalleryDescription);
        txtGalleryLatitude = (EditText) findViewById(R.id.txtGalleryLatitude);
        txtGalleryLongitude = (EditText) findViewById(R.id.txtGalleryLongitude);

        btnNewGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (lyNewGallery.getVisibility() == View.GONE) {
                    lyNewGallery.setVisibility(View.VISIBLE);
                } else {
                    lyNewGallery.setVisibility(View.GONE);
                }
            }
        });

        btnAddGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!txtGalleryName.getText().toString().isEmpty()) {
                    Galeria newGallery = new Galeria(txtGalleryName.getText().toString(),
                            txtGalleryDescription.getText().toString(), txtGalleryLatitude.getText().toString(),
                            txtGalleryLongitude.getText().toString());

                    LsGalerries.add(newGallery);
                    galleryAdapter.setData(LsGalerries);
                    lyNewGallery.setVisibility(View.GONE);
                } else {
                    lyNewGallery.setVisibility(View.GONE);
                }
            }
        });
    }
}
