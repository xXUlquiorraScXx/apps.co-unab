package com.pruebas.testprojectrecyclerview.Models;

/**
 * Created by Pedro on 13/02/2016.
 */
public class Galeria {
    private int id;
    private String nombre_galeria, descripcion, latitud, longitud;

    public Galeria() {
        setId(0);
        setNombre_galeria("");
        setDescripcion("");
    }

    public Galeria(String _nombre_galeria, String _descripcion, String _latitud, String _longitud) {
        setId(-1);
        setNombre_galeria(_nombre_galeria);
        setDescripcion(_descripcion);
        setLatitud(_latitud);
        setLongitud(_longitud);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre_galeria() {
        return nombre_galeria;
    }

    public void setNombre_galeria(String nombre_galeria) {
        this.nombre_galeria = nombre_galeria;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }
}
