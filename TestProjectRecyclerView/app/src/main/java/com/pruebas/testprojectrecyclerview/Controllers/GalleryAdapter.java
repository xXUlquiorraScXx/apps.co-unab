package com.pruebas.testprojectrecyclerview.Controllers;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.pruebas.testprojectrecyclerview.Models.Galeria;
import com.pruebas.testprojectrecyclerview.R;

import org.w3c.dom.ls.LSSerializer;

import java.util.List;

/**
 * Created by Pedro on 13/02/2016.
 */
public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.ViewHolder> {
    private Activity Acty;
    private List<Galeria> LsGalleries;

    public GalleryAdapter(Activity _Acty, List<Galeria> _LsGalleries) {
        this.Acty = _Acty;
        this.LsGalleries = _LsGalleries;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView imgVGalleryFront;
        private TextView lblGalleryName, lblGalleryDescription;

        public ViewHolder(View mView) {
            super(mView);

            this.imgVGalleryFront = (ImageView) mView.findViewById(R.id.imgVGalleryFront);
            this.lblGalleryName = (TextView) mView.findViewById(R.id.lblGalleryName);
            this.lblGalleryDescription = (TextView) mView.findViewById(R.id.lblGalleryDescription);
        }
    }

    @Override
    public int getItemCount() {
        return LsGalleries.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mVRoot = LayoutInflater.from(parent.getContext()).inflate(R.layout.ly_gallery_card, parent, false);

        ViewHolder vHolder = new ViewHolder(mVRoot);

        return vHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder vHolder, int pos) {
        LsGalleries.get(pos).setId(pos);
        final Galeria cGallery = LsGalleries.get(pos);

        Glide.with(Acty)
                .load("https://pmcvariety.files.wordpress.com/2014/04/36820.jpg?w=670&h=377&crop=1")
                .fitCenter()
                .into(vHolder.imgVGalleryFront);

        vHolder.lblGalleryName.setText(cGallery.getNombre_galeria());
        vHolder.lblGalleryDescription.setText(cGallery.getDescripcion());

        vHolder.imgVGalleryFront.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(Acty, "Abrir detalle de galeria " + cGallery.getId(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void setData(List<Galeria> _LsGaleria) {
        this.LsGalleries = null;
        this.LsGalleries = _LsGaleria;
        this.notifyDataSetChanged();
    }
}
